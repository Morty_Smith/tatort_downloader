#pragma once

#include <signal.h>
#include <iostream>
#include <string>

#include "TatortFS.h"

TatortFS* t = NULL;

int main(int argc, char*[]);

void signalHandler(int s);
std::string readLine(const std::string&);
