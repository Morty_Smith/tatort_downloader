#pragma once

#include <fstream>
#include <iostream>
#include <filesystem>
#include <string>
#include <set>
#include <map>
#include <regex>
#include "lib/curlpp/include/curlpp/cURLpp.hpp"
#include "lib/curlpp/include/curlpp/Easy.hpp"
#include "lib/curlpp/include/curlpp/Options.hpp"
#include "lib/curlpp/include/curlpp/Exception.hpp"


namespace fs = std::filesystem;

const std::regex tatortRegex("Tatort-(.+).mp4");

class TatortFS {
 private:
    std::set<std::string> knownFiles;
    std::map<const std::string, std::string> toDownload;
    const fs::path curFolder;
    const fs::path tmpFolder;

    void download(const std::string, const std::string);
    void move(const fs::path, const fs::path);
    void readtoDownload();
    void writetoDownload();
 public:
    explicit TatortFS(const std::string);
    ~TatortFS();
    bool add(const std::string, std::string);
    bool isKnown(const std::string);
    void startDownload();
};
