#include "main.h"

int main(int argc, char* argv[]) {
    signal(SIGINT, signalHandler);
    t = new TatortFS(argc > 1 ? argv[1]: "./");
    std::cout << "Type finish to start download of the Tatorte\n";
    std::string input = readLine("Tatort name: ");
    while (input.compare("finish") && input.compare("")) {
        if (!t->isKnown(input)) {
            t->add(input, readLine("Download url: "));
        } else {
            std::cout << "No Tatort added\n";
        }
        input = readLine("Tatort name: ");
    }
    t->startDownload();
    std::cout << "Finished downloading all the Tatorte\n";
    delete t;
    return 0;
}

std::string readLine(const std::string& output) {
    std::cout << output;
    std::string input;
    std::getline(std::cin, input);
    for (uint64_t i = 0; i < input.size(); i++) {
        if (input[i] == ' ') {
            input[i] = '_';
        }
    }
    if (input.size() > 2 && input[input.size() - 2] == '\n') {
        return (input.substr(0, input.size() - 2));
    } else {
        return input;
    }
}

void signalHandler(int s) {
    std::cout << "Recieved signal "<< s << "\n";
    if (t != NULL) {
        delete t;
        t = NULL;
    }
    exit(0);
}
