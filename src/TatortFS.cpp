#include "TatortFS.h"

TatortFS::TatortFS(const std::string folder): curFolder(fs::canonical(fs::path(folder))), tmpFolder(fs::temp_directory_path() / "Tatort") {
    if (!fs::exists(tmpFolder)) {
        fs::create_directory(tmpFolder);
    }
    for (auto& elem : fs::directory_iterator(this->curFolder, std::filesystem::directory_options::skip_permission_denied)) {
        if (fs::is_regular_file(elem)) {
            if (std::regex_match(elem.path().filename().string(), tatortRegex)) {
                this->knownFiles.insert(elem.path().filename().string().substr(7, elem.path().filename().string().length()-11));
            #ifdef DEBUG
                std::cout << "\x1B[1;32m+\033[0m " << elem.path().filename().string() << "\n";
            } else {
                std::cout << "\x1B[1;31m-\033[0m " << elem.path().filename().string() << std::endl;
            }
            #else
            }
            #endif
        }
    }
    std::cout << this->knownFiles.size() << " Tatorte gefunden\n";
    readtoDownload();
}

TatortFS::~TatortFS() {
    writetoDownload();
    fs::remove_all(this->tmpFolder);
}

void TatortFS::writetoDownload() {
    if (this->toDownload.size() > 0) {
        std::fstream out((this->curFolder / ".toDownload"), std::fstream::out);
        if (out.good()) {
            for (std::pair<const std::string, std::string> f : this->toDownload) {
                out.write((f.first + "\n").c_str(), f.first.length() + 1);
                out.write((f.second + "\n").c_str(), f.second.length() +1);
            }
        }
        out.close();
    } else {
        if (fs::exists(this->curFolder / ".toDownload")) {
            fs::remove((this->curFolder / ".toDownload"));
        }
    }
}

void TatortFS::readtoDownload() {
    std::ifstream in((this->curFolder / ".toDownload"), std::ifstream::in);
    if (in.is_open()) {
        std::string url;
        std::string name;
        while (getline(in, name) && getline(in, url)) {
                this->toDownload[name] = url;
        }
        in.close();
        fs::remove(this->curFolder / ".toDownload");
    }
}

bool TatortFS::add(const std::string name, std::string url) {
    if (url.compare("") && !isKnown(name)) {
            this->toDownload[name] = url;
            return true;
        }
    return false;
}

bool TatortFS::isKnown(const std::string name) {
    if (this->knownFiles.end() != this->knownFiles.find(name)) {
        return true;
    }
    if (this->toDownload.end() != this->toDownload.find(name)) {
        return true;
    }
    return false;
}

void TatortFS::move(const fs::path src, const fs::path dst) {
    std::cout << "Moving " << src << " to destination\n";
    try {
        fs::copy_file(src, dst);
        fs::remove(src);
    }
    catch (fs::filesystem_error & e) {
        std::cout << "Filesystem library didn't work, try using system command\n";
        system(("mv -f " + src.string() + " " + dst.string()).c_str());
    }
}

void TatortFS::download(const std::string filename, const std::string url) {
    std::ofstream downloadFile;
    downloadFile.open((this->tmpFolder / filename), std::ios::out);
    if (downloadFile.good()) {
        bool suc = false;
        do {
            std::cout << "Starting download of " << filename << "\n";
            try {
               curlpp::Easy request;
               request.setOpt(new curlpp::options::WriteStream(&downloadFile));
               request.setOpt(new curlpp::options::Url(url));
               request.perform();
               suc = true;
            }
            catch (curlpp::LogicError &e) {
              std::cerr << e.what() << std::endl;
              break;
            }
            catch (curlpp::RuntimeError &e) {
              std::cout << e.what() << std::endl;
            }
        }while (!suc);
        downloadFile.close();
        if (suc) {
            move(this->tmpFolder / filename, this->curFolder / filename);
        }
    } else {
        std::cout << "Error opening tmp file " << this->tmpFolder / filename << "\n";
    }
}

void TatortFS::startDownload() {
    for (std::map<const std::string, std::string>::iterator i = this->toDownload.begin(); i != this->toDownload.end();) {
        download("Tatort-" + i->first + ".mp4", i->second);
        this->knownFiles.insert(i->first);
        this->toDownload.erase(i++);
    }
}
